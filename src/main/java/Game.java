public class Game {


    private Player player1;
    private Player player2;
    private Machine machine;
    public Game(Player player1, Player player2, Machine machine) {
        this.player1 = player1;
        this.player2 = player2;
        this.machine = machine;
    }

    public void play() {

        Score score = machine.compute(player1.makeMove(), player2.makeMove());
        System.out.println("Player1 : " + score.getPlayer1() + " , Player2 : " + score.getPlayer2());
    }
}
