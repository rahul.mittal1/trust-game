import java.util.HashMap;
import java.util.Map;

public class Machine {

    private Map<String, Score> scoreMap;

    public Machine() {
        this.scoreMap = new HashMap<>();
        scoreMap.put(MoveType.COOPERATE.value + MoveType.COOPERATE.value, new Score(2, 2));
        scoreMap.put(MoveType.COOPERATE.value + MoveType.CHEAT.value, new Score(-1, 3));
        scoreMap.put(MoveType.CHEAT.value + MoveType.COOPERATE.value, new Score(3, -1));
        scoreMap.put(MoveType.CHEAT.value + MoveType.CHEAT.value, new Score(0, 0));
    }

    public Score compute(String player1Move, String player2Move) {
        return scoreMap.get(player1Move + player2Move);
    }
}
