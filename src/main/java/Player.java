import java.util.Scanner;

public class Player {

    private Scanner input;
    public Player(Scanner sc)
    {
        this.input = sc;
    }
    public String makeMove() {

        int userInput = input.nextInt();
        if(userInput == 1)
            return MoveType.COOPERATE.value;
        else if(userInput == 2)
            return  MoveType.CHEAT.value;
        return null;
    }
}
