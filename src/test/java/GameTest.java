import org.junit.Test;
import org.mockito.Matchers;
import org.mockito.Mockito;

import java.io.PrintStream;

import static org.junit.Assert.assertEquals;

public class GameTest {

    @Test
    public void shouldReturnScoreAfterFirstRound(){
        Player player1 = Mockito.mock(Player.class);
        Player player2 = Mockito.mock(Player.class);
        Mockito.when(player1.makeMove()).thenReturn(MoveType.COOPERATE.value);
        Mockito.when(player2.makeMove()).thenReturn(MoveType.COOPERATE.value);

        Machine machine = Mockito.mock(Machine.class);
        Mockito.when(machine.compute(MoveType.COOPERATE.value,MoveType.COOPERATE.value)).thenReturn(new Score(2,2));

        PrintStream out = Mockito.mock(PrintStream.class);
        System.setOut(out);

        Game game = new Game(player1,player2,machine);

        game.play();

        Mockito.verify(player1,Mockito.times(1)).makeMove();
        Mockito.verify(player2,Mockito.times(1)).makeMove();

        Mockito.verify(machine,Mockito.times(1)).compute(MoveType.COOPERATE.value,MoveType.COOPERATE.value);

        Mockito.verify(out,Mockito.times(1)).println("Player1 : 2 , Player2 : 2");

        Mockito.verify(out).println("Player1 : 2 , Player2 : 2");
    }
}
